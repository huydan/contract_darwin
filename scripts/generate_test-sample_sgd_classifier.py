

rootpath = "/Users/axa/Documents/darwin/contract_darwin"


import pandas as pd
import random



dataset=pd.read_csv(rootpath + '/data/input_sample_data.csv',sep=";")



index_lst = range(len(dataset))
random.shuffle(index_lst)

n_sample = 100
indx_sample_lst = [index_lst[i:i + n_sample] for i in xrange(0, len(index_lst), n_sample)]


for i,indx_sample in enumerate(indx_sample_lst):
    if i==0:
        dataset.iloc[indx_sample].to_pickle(rootpath  + "/data/interim/test_sample.p")
    else:
        dataset.iloc[indx_sample].to_pickle(rootpath  + "/data/interim/train_sample_" + str(i) + ".p")

    