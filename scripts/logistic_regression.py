import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import confusion_matrix, accuracy_score, \
    classification_report, precision_recall_curve, roc_curve, auc
from sklearn.preprocessing import label_binarize, OneHotEncoder, LabelEncoder
from datetime import date, timedelta, datetime




def calculate_roc(y_test, y_hat):
    # Compute ROC curve and ROC area for one class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    
    fpr, tpr, _ = roc_curve(y_test, y_hat)
    roc_auc = auc(fpr, tpr)

        
    return fpr, tpr, roc_auc


def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(iris.target_names))
    plt.xticks(tick_marks, iris.target_names, rotation=45)
    plt.yticks(tick_marks, iris.target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def plot_roc(fpr, tpr, roc_auc, color):
    # Plot all ROC curves
    plt.figure()
    plt.plot(fpr, tpr, color=color, lw=2,
             label='ROC curve of class {0} (area = {1:0.2f})'
             ''.format(i, roc_auc))

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Some extension of Receiver operating characteristic to multi-class')
    plt.legend(loc="lower right")
    plt.show()




# Balance the classes as there will be more no conversions than conversions
# Weights might need adjustment
# 0 - No conversion
# 1 - Conversion
class_weights = {0: 0.05, 1: 0.95}



# Here the hyper-parameters that can be tuned as C and class_weight.
# For C closer to 1 is better. For class weights as stated above is to have more examples of conversions
# than natually would appear
linreg = LogisticRegression(penalty='l1', C=0.5,
                            class_weight=class_weights)





dataset=pd.read_csv('/Users/axa/Documents/darwin/contract_darwin/data/input_sample_data.csv',sep=";")

dataset["birthday"] = pd.to_datetime(dataset["birthday"])
dataset["age"] = (date.today() - dataset["birthday"]) / timedelta(days=365.2425)

#Should we keep imputation by zero ?
dataset["age"] = dataset["age"].fillna(dataset["age"].mean())
dataset["age"]



# def g(row):
#     diff = date.today() - pd.to_datetime(row["birthday"]).date()
#     if (pd.isnull(diff)):
#         return np.datetime64('NaT')
#     else:
#         return diff // timedelta(days=365.2425)
    
# dataset = dataset[pd.notnull(dataset['birthday'])]
# dataset["age"] = dataset.apply(g, axis=1)
# dataset["age"] = dataset["age"].apply(pd.to_numeric, errors='coerce').fillna(0)
           

# def f(row):
#     if row['gender'] == 'M':
#         return 0
#     return 1

# dataset['gender'] = dataset.apply(f, axis=1)

dataset["gender"] = 1*(dataset["gender"]=="F")




maxpath = dataset['avg_total_path_days'].max()
dataset["avg_total_path_days"] = dataset["avg_total_path_days"].map(lambda x: x / maxpath)

print("---Total Days normalized----")

maxsteps = dataset['no_of_steps'].max()
dataset["no_of_steps"] = dataset["no_of_steps"].map(lambda x: x / maxsteps)

print("---No of Steps normalized----")


quality = {'A-Kunden':1,  'B-Kunden':2, 'C-Kunden':3}
dataset['kundenqualitaet'] = dataset['kundenqualitaet'].map(quality).fillna(0)

print("---Kundenquality normalized----")

haushold = {'MANN':1,  'FRAU':2, 'FAMK':3, 'UNBE':4, 'MAFR':5, 'MPHH':6, '':7}
dataset['haushaltsstruktur'] =  dataset['haushaltsstruktur'].map(haushold).fillna(0)


print("---Household normalized----")

dataset['transaction_id'] = dataset['transaction_id'].fillna(0)
#print(dataset['transaction_id'])
#pd.options.mode.chained_assignment = None  # default='warn'

dataset['transaction_id'] = np.where(dataset['transaction_id'] ==  0 , 0, 1)

print("---Transactions normalized----")


#dataset['transaction_id'].loc[~dataset['transaction_id'].isnull()] = 1  # not nan
#dataset['transaction_id'].loc[dataset['transaction_id'].isnull() ] = 0  # nan

initial_dataset = dataset.copy()


steps = {'mail':1,  'outbound':2, 'inbound':3}
dataset['step1'] =  dataset['step1'].map(steps).fillna(0)
dataset['step2'] =  dataset['step2'].map(steps).fillna(0)
dataset['step3'] =  dataset['step3'].map(steps).fillna(0)
dataset['step4'] =  dataset['step4'].map(steps).fillna(0)
dataset['step5'] =  dataset['step5'].map(steps).fillna(0)

#initial_dataset = dataset.copy()
con = dataset['transaction_id'].value_counts()[1]

nc =  dataset['transaction_id'].value_counts()[0]




# Here the hyper-parameters that can be tuned as C and class_weight.
# For C closer to 1 is better. For class weights as stated above is to have more examples of conversions
# than natually would appear

class_weights = {0: 0.05, 1: 0.95}

linreg = LogisticRegression(penalty='l1', C=0.5,
                            class_weight=class_weights)

y = list(dataset.transaction_id.values)
X = dataset.drop([
    'birthday', 
    'customer_id','transaction_id'], axis=1)

X = X.values


# Depending on the size of the dataset and number of features, the cv parameter should be adjusted
# In general you should try to have 10 * n_features < n_samples

scores = cross_val_score(linreg, X, y 
                         ,cv=10
                        )
scores

